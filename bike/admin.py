from django.contrib import admin

from .models import (
    Bike,
    BikeType,
)


@admin.register(BikeType)
class BikeTypeAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'type_name',
        'is_active',
        'created_at',
        'updated_at',
    ]
    list_filter = [
        'is_active',
        'created_at',
        'updated_at',
    ]
    search_fields = [
        'type_name',
    ]


@admin.register(Bike)
class BikeAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'bike_type',
        'name',
        'price',
        'remarks',
        'is_active',
        'created_at',
        'updated_at',
    ]
    list_filter = [
        'is_active',
        'created_at',
        'updated_at',
    ]
    search_fields = [
        'bike_type__type_name',
        'name',
    ]
