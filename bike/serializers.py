from rest_framework import serializers


class BikeRemarksSerializer(serializers.Serializer):
    price = serializers.CharField(max_length=20, required=False)
    color = serializers.CharField(max_length=20, required=False)


class BikeSerializer(serializers.Serializer):
    bike_type = serializers.CharField(max_length=10, required=False)
    name = serializers.CharField(max_length=50, required=False)
    remarks = BikeRemarksSerializer(many=True, required=False)
