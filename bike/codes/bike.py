from rest_framework import status
from applibs.responses.response import CodeObject


INVALID_INPUT = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_II_400",
    state_message={"en": "Invalid input", "bn": ""},
)

INTERNAL_SERVER_ERROR = CodeObject(
    http_status=status.HTTP_500_INTERNAL_SERVER_ERROR,
    state_code="DDRF_ISE_500",
    state_message={"en": "Internal server error", "bn": ""},
)

INVALID_TOKEN = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_IT_400",
    state_message={"en": "Invalid token", "bn": ""},
)

BIKE_TYPE_FOUND = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_BTF_200",
    state_message={"en": "Bike types found", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

BIKE_TYPE_NOT_FOUND = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_BTNF_200",
    state_message={"en": "Bike types not found", "bn": ""},
)

BIKE_CREATE_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_BCS_200",
    state_message={"en": "Bike create success", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

BIKE_CREATE_FAILED = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_BCF_400",
    state_message={"en": "Bike create failed", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

BIKE_PUBSUB_FOUND = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_BPSF_200",
    state_message={"en": "Bike pubsub found", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)
