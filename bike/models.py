from django.db import models
from django.utils import timezone
from django.db.models import F, Q, Case, When, Value
from django.db.models.functions import Upper, Lower


class BikeTypeManager(models.Manager):
    def get_bike_types(self):
        return self.using('default').filter().annotate(
            bike_name=F('bike_type_name__name')
        ).values(
            'id',
            'type_name',
            'bike_name'
        ).cache()

    def get_bike_type(self, bike_type_id):
        return self.using('default').filter(id=bike_type_id).last()


class BikeManager(models.Manager):
    def create_bike(self, data):
        return self.using('default').create(**data)


class BikeType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    objects = BikeTypeManager()

    def __str__(self):
        return self.type_name


class Bike(models.Model):
    id = models.BigAutoField(primary_key=True)
    bike_type = models.ForeignKey(BikeType, on_delete=models.CASCADE, related_name='bike_type_name')
    name = models.CharField(max_length=50)
    price = models.CharField(max_length=100, blank=True, null=True)
    remarks = models.JSONField(default=dict)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    objects = BikeManager()

    def __str__(self):
        return self.name
