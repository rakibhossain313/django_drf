from django.urls import path

from bike.views import (
    BikeTypeView,
    BikeCreateView,
    # CarPubSubKafkaPythonView,
    # CarPubSubConfluentKafkaView
)

urlpatterns = [
    # web apis
    path("types/", BikeTypeView.as_view()),
    path("create/", BikeCreateView.as_view()),
]
