import logging
import random

from django.utils.decorators import method_decorator
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from applibs.decorator.check_request_user import check_request_user
from applibs.decorator.get_logger import get_logger
from applibs.decorator.message_lang import lang

from applibs.responses.response import rd
from bike.codes import bike
from bike.models import (
    Bike,
    BikeType
)
from bike.serializers import BikeSerializer

logger = logging.getLogger("general")


class BikeTypeView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super(BikeTypeView, self).__init__()
        self.language = "en"
        self.data = None

    @method_decorator([
        lang,
        get_logger,
        check_request_user
    ])
    def get(self, request, **kwargs):
        try:
            self.data = BikeType.objects.get_bike_types()
            return Response(
                **rd.formatted_success_output(
                    code=bike.BIKE_TYPE_FOUND,
                    language=self.language,
                    data=self.data,
                )
            )
        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "Error occurred",
                "error": e
            })
            return Response(
                **rd.formatted_error_output(
                    code=bike.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )


class BikeCreateView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super(BikeCreateView, self).__init__()
        self.language = "en"
        self.data = None
        self.serializer = BikeSerializer

    @method_decorator([
        lang,
        get_logger,
        check_request_user
    ])
    def post(self, request, **kwargs):
        serialized_data = self.serializer(data=request.data)
        if not serialized_data.is_valid():
            return Response(
                **rd.formatted_error_output(
                    code=bike.INVALID_INPUT,
                    language=self.language,
                )
            )

        try:
            self.data = serialized_data.validated_data
            self.data["bike_type"] = BikeType.objects.get_bike_type(self.data['bike_type'])
            car_obj = Bike.objects.create(**self.data)
            if car_obj:
                return Response(
                    **rd.formatted_success_output(
                        code=bike.BIKE_CREATE_SUCCESS,
                        language=self.language,
                    )
                )
            return Response(
                **rd.formatted_error_output(
                    code=bike.BIKE_CREATE_FAILED,
                    language=self.language,
                )
            )
        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "Error occurred",
                "error": e
            })
            return Response(
                **rd.formatted_error_output(
                    code=bike.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )
