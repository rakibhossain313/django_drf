"""
Django settings for django_drf project.

Generated by 'django-admin startproject' using Django 5.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/5.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/5.1/ref/settings/
"""

import os
import environ
import pymysql
from pathlib import Path
from datetime import timedelta

from django_drf.logging_config import get_logger_config


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

env = environ.Env(
    DEBUG=(bool, False),
)
# reading .env file
env_file_path = (
    os.path.join(os.getenv("ENV_FILE_PATH", BASE_DIR), ".env")
)
environ.Env.read_env(env_file_path)

SECRET_KEY = env.str("SECRET_KEY")
VERIFYING_KEY = env.str("VERIFYING_KEY", None)
DEBUG = env.bool("DEBUG")
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS")
ALGORITHM = env.str("ALGORITHM")
MOCK = env.bool("MOCK", False)

CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS")

SELECT_DB = env.str("SELECT_DB", "default")

# SERVICE USERS
INTER_SERVICE_USER = env.str('INTER_SERVICE_USER', None)
INTER_SERVICE_PASSWORD = env.str('INTER_SERVICE_PASSWORD', None)

LOG_LEVEL = env.str("LOG_LEVEL", 'DEBUG')
LOGGER_ROOT_NAME = env.str("LOGGER_ROOT_NAME", 'general')

# Application definition

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

THIRD_PARTY_APPS = [
    'rest_framework',
    'django_extensions',
    'cacheops',
]

LOCAL_APPS = [
    'auth_management',
    'car',
    'bike',
    'wordpress',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "corsheaders.middleware.CorsMiddleware",  # cors related
    "cid.middleware.CidMiddleware",  # cid related
]

ROOT_URLCONF = 'django_drf.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / "templates"],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_drf.wsgi.application'


# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases

# for using mysql database
pymysql.install_as_MySQLdb()

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env.str("DEFAULT_DB_NAME"),
        "USER": env.str("DEFAULT_DB_USER"),
        "HOST": env.str("DEFAULT_DB_HOST"),
        "PORT": env.str("DEFAULT_DB_PORT"),
        "PASSWORD": env.str("DEFAULT_DB_PASSWORD"),
        "OPTIONS": env.dict("DEFAULT_DB_OPTIONS"),
    },
    "mysql_db": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": env.str("MYSQL_DB_NAME"),
        "USER": env.str("MYSQL_DB_USER"),
        "HOST": env.str("MYSQL_DB_HOST"),
        "PORT": env.str("MYSQL_DB_PORT"),
        "PASSWORD": env.str("MYSQL_DB_PASSWORD"),
        # "OPTIONS": env.dict("MYSQL_DB_OPTIONS"),
    },
    "wordpress_db": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": env.str("WP_DB_NAME"),
        "USER": env.str("WP_DB_USER"),
        "HOST": env.str("WP_DB_HOST"),
        "PORT": env.str("WP_DB_PORT"),
        "PASSWORD": env.str("WP_DB_PASSWORD"),
        # "OPTIONS": env.dict("WP_DB_OPTIONS"),
    }
}

DATABASE_ROUTERS = ['database_router.router.DatabaseRouter']

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework_simplejwt.authentication.JWTAuthentication",
    ],
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.IsAuthenticated",
    ],
    "EXCEPTION_HANDLER": "utils.custom_exception_handler.custom_exception_handler",
}

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(hours=1),  # Lifetime of the access token
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),  # Lifetime of the refresh token
    'ROTATE_REFRESH_TOKENS': env.bool('ROTATE_REFRESH_TOKENS'),  # Controls whether refresh tokens should be rotated
    'BLACKLIST_AFTER_ROTATION': env.bool('BLACKLIST_AFTER_ROTATION'),  # If True, refresh tokens are added to the blacklist after rotation
    'UPDATE_LAST_LOGIN': env.bool('UPDATE_LAST_LOGIN'),  # Whether to update the user's last login time upon authentication
    'ALGORITHM': ALGORITHM,  # Algorithm used for encoding the JWT
    'SIGNING_KEY': SECRET_KEY,  # Secret key used to sign the JWTs (default is Django's SECRET_KEY)
    'VERIFYING_KEY': VERIFYING_KEY,  # Public key used to verify the JWTs (if using RS256 or other asymmetric algorithms)
    'AUTH_HEADER_TYPES': env.list('AUTH_HEADER_TYPES'),  # How the JWT is passed in the Authorization header
    'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',  # The HTTP header in which the token is passed
    # "TOKEN_OBTAIN_SERIALIZER": "auth_management.serializers.LoginSerializer",
}

SWAGGER_SETTINGS = {
    "SECURITY_DEFINITIONS": {
        "Bearer": {"type": "apiKey", "name": "Authorization", "in": "header"}
    },
    "JSON_EDITOR": True,
}

AUTH_USER_MODEL = 'auth_management.User'

# Password validation
# https://docs.djangoproject.com/en/4.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Cache settings
CACHEOPS_REDIS = env.str('CACHES_LOCATION')
CACHEOPS_ENABLED = env.bool('CACHEOPS_ENABLED', True)
CACHEOPS_DEGRADE_ON_FAILURE = env.bool('CACHEOPS_DEGRADE_ON_FAILURE', True)  # Prevents app from breaking if Redis is down
CACHEOPS_TIMEOUT_MINUTES = env.int('CACHEOPS_TIMEOUT_MINUTES')
CACHEOPS = {
    '*.*': {'timeout': CACHEOPS_TIMEOUT_MINUTES * 60},
}

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': CACHEOPS_REDIS,
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    }
}


LOGGING = get_logger_config(LOGGER_ROOT_NAME, LOG_LEVEL)

# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.2/howto/static-files/

STATIC_URL = 'static/'
STATIC_ROOT = BASE_DIR / 'staticfiles'

# Default primary key field type
# https://docs.djangoproject.com/en/4.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
