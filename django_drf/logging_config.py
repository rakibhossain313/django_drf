def get_logger_config(root_name: str, level: str) -> dict:
    return {
        "version": 1,
        "disable_existing_loggers": False,
        "filters": {
            "correlation": {"()": "cid.log.CidContextFilter"},
        },
        "formatters": {
            "console": {
                "format": "{levelname:<8s} | {cid} | {asctime} | {pathname}:{lineno} | {message}",
                "style": "{",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "console",
                "filters": ["correlation"],
            },
        },
        "loggers": {
            root_name: {
                "level": level,
                "handlers": ["console"],
                "propagate": False,
                "filters": ["correlation"],
            },
        },
    }
