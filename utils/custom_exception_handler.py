from rest_framework import status
from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    language = context['request'].headers.get('Accept-Language')

    # Check if the exception is an authentication error
    if response is not None and not status.is_success(response.status_code):
        custom_response_data = {
            "code": response.status_code,
            "lang": language,
            "message": {"en": "Invalid token / unauthorised entry!", "bn": ""},
        }
        response.status = response.status_code
        response.data = custom_response_data

    return response
