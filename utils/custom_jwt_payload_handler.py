def custom_jwt_payload_handler(user):
    """
    Custom JWT payload handler.
    """
    # Customize the payload according to your requirements
    user_payload = {
        "user_id": user.id,
        "username": user.username,
        "email": user.email,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "is_active": user.is_active,
    }
    return user_payload
