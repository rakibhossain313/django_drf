from datetime import datetime
from datetime import timedelta

from django.contrib import admin
from django.utils.timezone import now
from django.utils.timezone import make_aware
from django.utils.dateparse import parse_datetime

from .models import (
    WpCommentmeta,
    WpComments,
    WpLinks,
    WpOptions,
    WpPostmeta,
    WpPosts,
    WpTermRelationships,
    WpTermTaxonomy,
    WpTermmeta,
    WpTerms,
    WpUsermeta,
    WpUsers,
)


@admin.register(WpCommentmeta)
class WpCommentmetaAdmin(admin.ModelAdmin):
    list_display = [
        'meta_id',
        'comment_id',
        'meta_key',
        'meta_value',
    ]


@admin.register(WpComments)
class WpCommentsAdmin(admin.ModelAdmin):
    list_display = [
        'comment_id',
        'comment_post_id',
        'comment_author',
        'comment_author_email',
        'comment_author_url',
        'comment_author_ip',
        'comment_date',
        'comment_date_gmt',
        'comment_content',
        'comment_karma',
        'comment_approved',
        'comment_agent',
        'comment_type',
        'comment_parent',
        'user_id',
    ]
    list_filter = [
        'comment_date',
        'comment_date_gmt',
    ]


@admin.register(WpLinks)
class WpLinksAdmin(admin.ModelAdmin):
    list_display = [
        'link_id',
        'link_url',
        'link_name',
        'link_image',
        'link_target',
        'link_description',
        'link_visible',
        'link_owner',
        'link_rating',
        'link_updated',
        'link_rel',
        'link_notes',
        'link_rss',
    ]
    list_filter = [
        'link_updated',
    ]


@admin.register(WpOptions)
class WpOptionsAdmin(admin.ModelAdmin):
    list_display = [
        'option_id',
        'option_name',
        'option_value',
        'autoload',
    ]


@admin.register(WpPostmeta)
class WpPostmetaAdmin(admin.ModelAdmin):
    list_display = [
        'meta_id',
        'post_id',
        'meta_key',
        'meta_value',
    ]


class PostDateFilter(admin.SimpleListFilter):
    title = 'Post Date'
    parameter_name = 'post_date'

    def lookups(self, request, model_admin):
        return [
            ('today', 'Today'),
            ('past_week', 'Past Week'),
            ('past_month', 'Past Month'),
            ('past_year', 'Past Year'),
        ]

    def queryset(self, request, queryset):
        """Manually parse string dates to datetime before filtering"""
        today = now().date()
        past_week = now() - timedelta(days=7)
        past_month = now() - timedelta(days=30)
        past_year = now() - timedelta(days=365)

        # Convert string dates to datetime objects
        for obj in queryset:
            if isinstance(obj.post_date, str):
                obj.post_date = parse_datetime(obj.post_date)

        if self.value() == 'today':
            return queryset.filter(post_date__date=today)
        elif self.value() == 'past_week':
            return queryset.filter(post_date__gte=past_week)
        elif self.value() == 'past_month':
            return queryset.filter(post_date__gte=past_month)
        elif self.value() == 'past_year':
            return queryset.filter(post_date__gte=past_year)

        return queryset


class PostDateGmtFilter(admin.SimpleListFilter):
    title = 'Post Date GMT'
    parameter_name = 'post_date_gmt'

    def lookups(self, request, model_admin):
        return [
            ('today', 'Today'),
            ('past_week', 'Past Week'),
            ('past_month', 'Past Month'),
            ('past_year', 'Past Year'),
        ]

    def queryset(self, request, queryset):
        today = now().date()
        past_week = now() - timedelta(days=7)
        past_month = now() - timedelta(days=30)
        past_year = now() - timedelta(days=365)

        # Convert string dates to datetime objects
        for obj in queryset:
            if isinstance(obj.post_date_gmt, str):
                obj.post_date_gmt = parse_datetime(obj.post_date_gmt)

        if self.value() == 'today':
            return queryset.filter(post_date_gmt__date=today)
        elif self.value() == 'past_week':
            return queryset.filter(post_date_gmt__gte=past_week)
        elif self.value() == 'past_month':
            return queryset.filter(post_date_gmt__gte=past_month)
        elif self.value() == 'past_year':
            return queryset.filter(post_date_gmt__gte=past_year)

        return queryset


@admin.register(WpPosts)
class WpPostsAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'post_author',
        'get_post_date',
        'get_post_date_gmt',
        'post_content',
        'post_title',
        'post_excerpt',
        'post_status',
        'comment_status',
        'ping_status',
        'post_password',
        'post_name',
        'to_ping',
        'pinged',
        'get_post_modified',
        'get_post_modified_gmt',
        'post_content_filtered',
        'post_parent',
        'guid',
        'menu_order',
        'post_type',
        'post_mime_type',
        'comment_count',
    ]
    list_filter = [
        "post_date",
        "post_date_gmt",
        'post_modified',
        'post_modified_gmt',
    ]
    # list_filter = [
    #     PostDateFilter,
    #     PostDateGmtFilter,
    # ]

    def get_post_date(self, obj):
        # print("post_date ==========>", obj.post_date)

        """Handle invalid MySQL datetime (0000-00-00 00:00:00)"""
        if obj.post_date:
            return obj.post_date
        return "-"  # Display '-' instead of an invalid date

    def get_post_date_gmt(self, obj):
        # print("post_date_gmt ==========>", obj.post_date_gmt)

        """Handle invalid MySQL datetime (0000-00-00 00:00:00)"""
        if obj.post_date_gmt:
            return obj.post_date_gmt
        return "-"  # Display '-' instead of an invalid date

    def get_post_modified(self, obj):
        # print("post_modified ==========>", obj.post_modified)

        """Handle invalid MySQL datetime (0000-00-00 00:00:00)"""
        if obj.post_modified:
            return obj.post_modified
        return "-"  # Display '-' instead of an invalid date

    def get_post_modified_gmt(self, obj):
        # print("post_modified_gmt ==========>", obj.post_modified_gmt)

        """Handle invalid MySQL datetime (0000-00-00 00:00:00)"""
        if obj.post_modified_gmt:
            return obj.post_modified_gmt
        return "-"  # Display '-' instead of an invalid date

    get_post_date.admin_order_field = "post_date"
    get_post_date.short_description = "Post Date"

    get_post_date_gmt.admin_order_field = "post_date_gmt"
    get_post_date_gmt.short_description = "Post Date GMT"

    get_post_modified.admin_order_field = "post_modified"
    get_post_modified.short_description = "Post Modified"

    get_post_modified_gmt.admin_order_field = "post_modified_gmt"
    get_post_modified_gmt.short_description = "Post Modified GMT"


@admin.register(WpTermRelationships)
class WpTermRelationshipsAdmin(admin.ModelAdmin):
    list_display = [
        'object_id',
        'term_taxonomy_id',
        'term_order',
    ]


@admin.register(WpTermTaxonomy)
class WpTermTaxonomyAdmin(admin.ModelAdmin):
    list_display = [
        'term_taxonomy_id',
        'term_id',
        'taxonomy',
        'description',
        'parent',
        'count',
    ]


@admin.register(WpTermmeta)
class WpTermmetaAdmin(admin.ModelAdmin):
    list_display = [
        'meta_id',
        'term_id',
        'meta_key',
        'meta_value',
    ]


@admin.register(WpTerms)
class WpTermsAdmin(admin.ModelAdmin):
    list_display = [
        'term_id',
        'name',
        'slug',
        'term_group',
    ]
    search_fields = [
        'name',
        'slug',
    ]
    prepopulated_fields = {
        'slug': ['name']
    }


@admin.register(WpUsermeta)
class WpUsermetaAdmin(admin.ModelAdmin):
    list_display = [
        'umeta_id',
        'user_id',
        'meta_key',
        'meta_value',
    ]


@admin.register(WpUsers)
class WpUsersAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'user_login',
        'user_pass',
        'user_nicename',
        'user_email',
        'user_url',
        'user_registered',
        'user_activation_key',
        'user_status',
        'display_name',
    ]
    list_filter = [
        'user_registered',
    ]
