class DatabaseRouter:
    def db_for_read(self, model, **hints):
        """
        Directs read operations for specific models to different databases.
        """
        if model._meta.app_label == 'car':
            return 'default'
        elif model._meta.app_label == 'bike':
            return 'default'
        elif model._meta.app_label == 'wordpress':
            return 'wordpress_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Directs write operations for specific models to different databases.
        """
        if model._meta.app_label == 'car':
            return 'default'
        elif model._meta.app_label == 'bike':
            return 'default'
        elif model._meta.app_label == 'wordpress':
            return 'wordpress_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if both objects are from the same database.
        """
        if obj1._state.db == obj2._state.db:
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the 'app1' models appear only in the 'db1' database
        and 'app2' models appear only in the 'db2' database.
        """
        if app_label == 'car':
            return db == 'default'
        elif app_label == 'bike':
            return db == 'default'
        elif app_label == 'wordpress':
            return db == 'wordpress_db'
        return None
