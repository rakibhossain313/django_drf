from django.urls import path

from car.views.bd_hierarchy_info_views import BDHierarchyInfoView
from car.views.car_views import (
    CarTypeView,
    CarCreateView,
)
from car.views.pub_sub_views import (
    PublishMessageKafkaView,
    SubscribeMessageKafkaView,
    PublishMessageConfluentKafkaView,
    SubscribeMessageConfluentKafkaView,
)

urlpatterns = [
    # web apis
    path("types/", CarTypeView.as_view()),
    path("create/", CarCreateView.as_view()),
    path("bd/hierarchy/info/", BDHierarchyInfoView.as_view()),
    path("publish-message/kafka/", PublishMessageKafkaView.as_view()),
    path("subscribe-message/kafka/", SubscribeMessageKafkaView.as_view()),
    path("publish-message/confluent-kafka/", PublishMessageConfluentKafkaView.as_view()),
    path("subscribe-message/confluent-kafka/", SubscribeMessageConfluentKafkaView.as_view()),
]
