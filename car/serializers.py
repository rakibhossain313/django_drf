from rest_framework import serializers


class CarRemarksSerializer(serializers.Serializer):
    price = serializers.CharField(max_length=20, required=False)
    color = serializers.CharField(max_length=20, required=False)


class CarSerializer(serializers.Serializer):
    car_type = serializers.CharField(max_length=10, required=False)
    name = serializers.CharField(max_length=50, required=False)
    price = serializers.CharField(max_length=50, required=False)
    remarks = CarRemarksSerializer(many=True, required=False)
