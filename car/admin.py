from django.contrib import admin
from django.core.paginator import Paginator
from django.db.models import Sum, FloatField
from django.db.models.functions import Cast
from .models import (
    Car,
    CarType,
    BangladeshHierarchyInfo,
)


@admin.register(CarType)
class CarTypeAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'type_name',
        'is_active',
        'created_at',
        'updated_at',
    ]
    list_filter = [
        'is_active',
        'created_at',
        'updated_at',
    ]
    search_fields = [
        'type_name',
    ]


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'car_type',
        'name',
        'price',
        'remarks',
        'is_active',
        'created_at',
        'updated_at',
    ]
    list_filter = [
        'is_active',
        'created_at',
        'updated_at',
    ]
    search_fields = [
        'car_type__type_name',
        'name',
    ]

    change_list_template = "admin/car_list.html"

    def calculate_all_page_total_price(self, request):
        qs = self.get_queryset(request).filter(is_active=True).order_by('-id')  # Sorting by price descending
        # print("qs: ", qs)

        # Sum the "price" field
        total_price = qs.annotate(
            price_as_float=Cast("price", FloatField())
        ).aggregate(
            total_price_sum=Sum("price_as_float")
        )["total_price_sum"] or 0.0

        return total_price

    def calculate_per_page_total_price(self, request):
        page_number = request.GET.get('p', 1)
        # print("page number: ", page_number)

        qs = self.get_queryset(request).order_by('-id')  # Sorting by price descending
        # print("qs: ", qs)

        # Apply pagination to the queryset
        paginator = Paginator(qs, self.list_per_page)  # self.list_per_page gives the page size
        # print("paginator: ", paginator)
        page = paginator.get_page(page_number)
        # print("page: ", page)

        # Get the queryset for the current page
        page_queryset = page.object_list
        # print("page queryset: ", page_queryset)
        # print("page queryset count: ", page_queryset.count())

        total_price = 0.0
        for data in page_queryset:
            # print(f"Price: {data.price}, Active status: {data.is_active}")
            if data.is_active:
                total_price += float(data.price)

        return total_price

    def changelist_view(self, request, extra_context=None):
        request_params = list(request.GET.keys())
        # print("request params: ", request_params)

        if not request_params or 'p' in request_params:
            total_price = self.calculate_per_page_total_price(request=request)
        else:
            total_price = self.calculate_all_page_total_price(request=request)

        """Modify the context to add total sum row"""
        extra_context = extra_context or {}

        # Add total to the context
        extra_context["summary_total"] = total_price
        extra_context["summary_demo_total"] = 0.0

        return super().changelist_view(request, extra_context=extra_context)


@admin.register(BangladeshHierarchyInfo)
class BangladeshHierarchyInfoAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'division',
        'district',
        'upazilla',
        'union',
    ]
    search_fields = [
        'division',
        'district',
        'upazilla',
        'union',
    ]
