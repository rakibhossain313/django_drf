import logging
import random
import time

from django.utils.decorators import method_decorator
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)
from rest_framework.renderers import (
    JSONRenderer,
    TemplateHTMLRenderer
)
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from applibs.decorator.message_lang import lang
from applibs.decorator.get_logger import get_logger
from applibs.decorator.check_request_user import check_request_user

from applibs.kafka_pub_sub.confluent_kafka_consumer import consume_messages_by_confluent_kafka
from applibs.kafka_pub_sub.confluent_kafka_producer import send_message_by_confluent_kafka
from applibs.kafka_pub_sub.python_kafka_consumer import consume_messages_by_python_kafka
from applibs.kafka_pub_sub.python_kafka_producer import send_message_by_python_kafka

from applibs.responses.response import rd
from car.codes import car

logger = logging.getLogger("general")


class PublishMessageKafkaView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super().__init__()
        self.language = "en"
        self.data = list()

    @method_decorator([
        lang,
        get_logger,
        check_request_user
    ])
    def get(self, request, **kwargs):
        try:
            data = dict()
            data["messages"] = list()

            data['username'] = request.user.username
            data['timestamp'] = time.time()
            for i in range(5):
                data['messages'].append(f"🔔 Hello {data['username']}, you have a new notification-{i+1}!")

            status, response = send_message_by_python_kafka(topic='user_notifications', data=data)
            # sleep(1)

            return Response(
                **rd.formatted_success_output(
                    code=car.PUBLISH_MSG_PYTHON_KAFKA_SUCCESS,
                    language=self.language,
                    data=response,
                )
            )
        except Exception as e:
            logger.error({
                "request": f"{self.request.log}",
                "response": f"Error occurred!",
                "error": f"{str(e)}"
            })
            return Response(
                **rd.formatted_error_output(
                    code=car.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )


class SubscribeMessageKafkaView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super().__init__()
        self.language = "en"
        self.data = list()

    @method_decorator([
        lang,
        get_logger,
        check_request_user
    ])
    def get(self, request, **kwargs):
        try:
            status, response = consume_messages_by_python_kafka(topics=['user_notifications'], group_id='notification_group')
            return Response(
                **rd.formatted_success_output(
                    code=car.SUBSCRIBE_MSG_PYTHON_KAFKA_SUCCESS,
                    language=self.language,
                    data=response,
                )
            )
        except Exception as e:
            logger.error({
                "request": f"{self.request.log}",
                "response": f"Error occurred!",
                "error": f"{str(e)}"
            })
            return Response(
                **rd.formatted_error_output(
                    code=car.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )


class PublishMessageConfluentKafkaView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super().__init__()
        self.language = "en"
        self.data = None

    @method_decorator([
        lang,
        get_logger,
        check_request_user
    ])
    def get(self, request, **kwargs):
        try:
            data = dict()
            data["messages"] = list()

            data['username'] = request.user.username
            data['timestamp'] = time.time()
            for i in range(5):
                data['messages'].append(f"🔔 Hello {data['username']}, you have a new notification-{i + 1}!")

            status, response =send_message_by_confluent_kafka(key='partition-1', topic='user_notifications', value=data)

            return Response(
                **rd.formatted_success_output(
                    code=car.PUBLISH_MSG_CONFLUENT_KAFKA_SUCCESS,
                    language=self.language,
                    data=response,
                )
            )
        except Exception as e:
            logger.error({
                "request": f"{self.request.log}",
                "response": f"Error occurred!",
                "error": f"{str(e)}"
            })
            return Response(
                **rd.formatted_error_output(
                    code=car.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )


class SubscribeMessageConfluentKafkaView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super().__init__()
        self.language = "en"
        self.data = None

    @method_decorator([
        lang,
        get_logger,
        check_request_user
    ])
    def get(self, request, **kwargs):
        try:
            status, response = consume_messages_by_confluent_kafka(topics=['user_notifications'], group_id='notification_group')

            return Response(
                **rd.formatted_success_output(
                    code=car.SUBSCRIBE_MSG_CONFLUENT_KAFKA_SUCCESS,
                    language=self.language,
                    data=response,
                )
            )
        except Exception as e:
            logger.error({
                "request": f"{self.request.log}",
                "response": f"Error occurred!",
                "error": f"{str(e)}"
            })
            return Response(
                **rd.formatted_error_output(
                    code=car.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )
