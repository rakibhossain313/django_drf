import logging

from django.utils.decorators import method_decorator
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)
from rest_framework.renderers import (
    JSONRenderer,
    TemplateHTMLRenderer
)
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from applibs.decorator.check_request_user import check_request_user
from applibs.decorator.get_logger import get_logger
from applibs.decorator.message_lang import lang

from applibs.responses.response import rd
from car.codes import car
from car.models import (
    BangladeshHierarchyInfo
)

logger = logging.getLogger("general")


class BDHierarchyInfoView(APIView):
    authentication_classes = []
    permission_classes = [AllowAny]

    def __init__(self):
        super().__init__()
        self.language = "en"
        self.data = None

    @method_decorator([
        lang,
        get_logger,
        # check_request_user
    ])
    def get(self, request, **kwargs):
        try:
            params = request.query_params
            self.data = BangladeshHierarchyInfo.objects.get_bd_hierarchy_info(params=params)
            return Response(
                **rd.formatted_success_output(
                    code=car.BD_HIERARCHY_INFO_FOUND,
                    language=self.language,
                    data=self.data,
                )
            )
        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "Error occurred",
                "error": e
            })
            return Response(
                **rd.formatted_error_output(
                    code=car.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )
