import logging
from my_package import (
    greet,
    hello_world
)

from django.utils.decorators import method_decorator
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)
from rest_framework.renderers import (
    JSONRenderer,
    TemplateHTMLRenderer
)
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from applibs.decorator.check_request_user import check_request_user
from applibs.decorator.get_logger import get_logger
from applibs.decorator.message_lang import lang

from applibs.responses.response import rd
from car.codes import car
from car.models import (
    Car,
    CarType,
)
from car.serializers import CarSerializer

logger = logging.getLogger("general")


class CarTypeView(APIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    renderer_classes = [
        JSONRenderer,
        TemplateHTMLRenderer,
    ]

    template_name = 'car.html'

    def __init__(self):
        super().__init__()
        self.language = "en"
        self.data = None

    @method_decorator([
        lang,
        get_logger,
        # check_request_user
    ])
    def get(self, request, **kwargs):
        try:
            hello_world()
            print(greet('Rakib'))

            self.data = CarType.objects.get_car_types()
            return Response(
                **rd.formatted_success_output(
                    code=car.CAR_TYPE_FOUND,
                    language=self.language,
                    data=self.data,
                )
            )
        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "Error occurred",
                "error": e
            })
            return Response(
                **rd.formatted_error_output(
                    code=car.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )


class CarCreateView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super().__init__()
        self.language = "en"
        self.data = None
        self.serializer = CarSerializer

    @method_decorator([
        lang,
        get_logger,
        check_request_user
    ])
    def post(self, request, **kwargs):
        serialized_data = self.serializer(data=request.data)
        if not serialized_data.is_valid():
            return Response(
                **rd.formatted_error_output(
                    code=car.INVALID_INPUT,
                    language=self.language,
                )
            )

        try:
            self.data = serialized_data.validated_data
            self.data["car_type"] = CarType.objects.get_car_type(self.data['car_type'])

            # for i in range(200):
            #     _ = Car.objects.create_car(**self.data)
            #
            # return Response(
            #     **rd.formatted_success_output(
            #         code=car.CAR_CREATE_SUCCESS,
            #         language=self.language,
            #     )
            # )

            car_obj = Car.objects.create_car(**self.data)
            if car_obj:
                return Response(
                    **rd.formatted_success_output(
                        code=car.CAR_CREATE_SUCCESS,
                        language=self.language,
                    )
                )
            return Response(
                **rd.formatted_error_output(
                    code=car.CAR_CREATE_FAILED,
                    language=self.language,
                )
            )
        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "Error occurred",
                "error": e
            })
            return Response(
                **rd.formatted_error_output(
                    code=car.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )
