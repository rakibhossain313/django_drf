from rest_framework import status
from applibs.responses.response import CodeObject


INVALID_INPUT = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_II_400",
    state_message={"en": "Invalid input", "bn": ""},
)

INTERNAL_SERVER_ERROR = CodeObject(
    http_status=status.HTTP_500_INTERNAL_SERVER_ERROR,
    state_code="DDRF_ISE_500",
    state_message={"en": "Internal server error", "bn": ""},
)

INVALID_TOKEN = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_IT_400",
    state_message={"en": "Invalid token", "bn": ""},
)

CAR_TYPE_FOUND = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_CTF_200",
    state_message={"en": "Car types found", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

CAR_TYPE_NOT_FOUND = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_CTNF_200",
    state_message={"en": "Car types not found", "bn": ""},
)

CAR_CREATE_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_CCS_200",
    state_message={"en": "Car create success", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

CAR_CREATE_FAILED = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_CCF_400",
    state_message={"en": "Car create failed", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

BD_HIERARCHY_INFO_FOUND = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_BDHIF_200",
    state_message={"en": "Bangladesh Hierarchy Info found", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

PUBLISH_MSG_PYTHON_KAFKA_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_PMPKS_200",
    state_message={"en": "Publish message to python kafka success!", "bn": ""},
)

SUBSCRIBE_MSG_PYTHON_KAFKA_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_SMPKS_200",
    state_message={"en": "Subscribe message to python kafka success!", "bn": ""},
)

PUBLISH_MSG_CONFLUENT_KAFKA_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_PMCKS_200",
    state_message={"en": "Publish message to confluent kafka success!", "bn": ""},
)

SUBSCRIBE_MSG_CONFLUENT_KAFKA_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_SMCKS_200",
    state_message={"en": "Subscribe message to confluent kafka success!", "bn": ""},
)
