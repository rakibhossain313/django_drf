from django.db import models
from django.utils import timezone
from django.db.models import F, Q, Case, When, Value
from django.db.models.functions import Upper, Lower

from sqlalchemy import text, create_engine
from sqlalchemy.orm import sessionmaker


class CarTypeManager(models.Manager):
    def get_car_types(self):
        # Use the SQLAlchemy engine you configured
        engine = create_engine(
            'postgresql+psycopg2://upay:123456@localhost/django_drf',
            pool_size=20,
            max_overflow=10,
            pool_timeout=30,
            pool_recycle=3600
        )

        # Create SQLAlchemy engine and session
        Session = sessionmaker(bind=engine)
        session = Session()

        # Execute raw SQL to fetch book IDs
        sql = text("SELECT * FROM car_cartype")
        result = session.execute(sql)

        # Extract book IDs from the result
        car_ids = [row['id'] for row in result.mappings().all()]
        print("Car IDs for SQLAlchemy: \n", car_ids)

        return self.filter(id__in=car_ids).annotate(
            car_name=F('car_type_name__name')
        ).values(
            'id',
            'type_name',
            'car_name'
        ).cache()

        # return self.filter().using('default').annotate(
        #     car_name=F('car_type_name__name')
        # ).values(
        #     'id',
        #     'type_name',
        #     'car_name'
        # ).cache()

    def get_car_type(self, car_type_id):
        return self.using('default').filter(id=car_type_id).last()


class CarManager(models.Manager):
    def create_car(self, **data):
        return self.using('default').create(**data)


class CarType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    objects = CarTypeManager()

    def __str__(self):
        return self.type_name


class Car(models.Model):
    id = models.BigAutoField(primary_key=True)
    car_type = models.ForeignKey(CarType, on_delete=models.CASCADE, related_name='car_type_name')
    name = models.CharField(max_length=50)
    price = models.CharField(max_length=100, blank=True, null=True)
    remarks = models.JSONField(default=dict)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    objects = CarManager()

    def __str__(self):
        return self.name


class BangladeshHierarchyInfoManager(models.Manager):
    def get_bd_hierarchy_info(self, params):
        distinct_key = 'district'
        division = params.get('division', '').lower()
        district = params.get('district', '').lower()
        upazilla = params.get('upazilla', '').lower()
        union = params.get('union', '').lower()

        query = Q()

        if division:
            query &= Q(division__exact=division)
        if district:
            query &= Q(district__exact=district)
        if upazilla:
            query &= Q(upazilla__exact=upazilla)
        if union:
            query &= Q(union__exact=union)

        query_data = self.filter(query).values()
        if division and not district and not upazilla:
            return query_data.distinct(distinct_key)
        return query_data


class BangladeshHierarchyInfo(models.Model):
    division = models.CharField(max_length=50, blank=True, null=True)
    district = models.CharField(max_length=50, blank=True, null=True)
    upazilla = models.CharField(max_length=100, blank=True, null=True)
    union = models.CharField(max_length=100, blank=True, null=True)

    objects = BangladeshHierarchyInfoManager()

    def __str__(self):
        return self.division
