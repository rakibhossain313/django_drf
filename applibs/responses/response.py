from typing import Any


class CodeObject:

    def __init__(self, **kwargs):
        self._http_status = kwargs.get('http_status', 500)
        self._state_code = kwargs.get('state_code', 'MO_500')
        self._state_message = kwargs.get('state_message', {'en': 'No message defined'})

    def http_status(self) -> int:
        return self._http_status

    def set_http_status(self, status):
        self._http_status = status

    def state_code(self) -> str:
        return self._state_code

    def set_state_code(self, state_code):
        self._state_code = state_code

    def state_message(self) -> dict:
        return self._state_message

    def set_state_message(self, message):
        self._state_message = message

    def is_http_error_status(self) -> bool:
        return 400 <= self._http_status <= 599

    def is_http_success_status(self) -> bool:
        return 200 <= self._http_status <= 299


class ResponseData:

    def formatted_state_message(self, code: CodeObject, language: str = 'en') -> str:
        data: dict = code.state_message()
        if language in data:
            return data.get(language)
        else:
            return data.get('en', '')

    def formatted_error_output(self, code: CodeObject, language: str = 'en', errors: Any = None, **kwargs) -> dict:
        if not code.is_http_error_status():
            raise ValueError('The http status code is not in error range')

        output = dict()
        output['status'] = code.http_status()
        output['data'] = {}
        if kwargs:
            output['data'].update(kwargs)
        output['data']['code'] = code.state_code()
        output['data']['lang'] = language
        output['data']['message'] = self.formatted_state_message(code, language)
        if errors is not None:
            output['data']['errors'] = errors
        return output

    def formatted_success_output(self, code: CodeObject, language: str = 'en', data: Any = None, **kwargs) -> dict:
        if not code.is_http_success_status():
            raise ValueError('The http status code is not in success range.')

        output = dict()
        output['status'] = code.http_status()
        output['data'] = {}
        if kwargs:
            output['data'].update(kwargs)
        output['data']['code'] = code.state_code()
        output['data']['lang'] = language
        output['data']['message'] = self.formatted_state_message(code, language)
        if data is not None:
            output['data']['data'] = data
        return output


rd = ResponseData()
