from collections import OrderedDict

from rest_framework.pagination import LimitOffsetPagination, _positive_int
from rest_framework.response import Response
from django.utils.translation import gettext_lazy as _


class LimitOffsetOrAllPagination(LimitOffsetPagination):

    page_query_param = 'page'
    page_query_description = _('The initial index from which to return the results.')

    def get_page(self, request):
        try:
            return _positive_int(
                request.query_params[self.page_query_param],
            )
        except (KeyError, ValueError):
            return None

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.count),
            ('next', self.get_next_link() if self.get_next_link() else ""),
            ('previous', self.get_previous_link() if self.get_previous_link() else ""),
            ('results', data)
        ]))

    def paginate_queryset(self, queryset, request, view=None):
        self.count = self.get_count(queryset)
        self.limit = self.get_limit(request)
        self.offset = self.get_offset(request)
        self.page = self.get_page(request)

        if self.limit is None:
            self.limit = self.count
            self.display_page_controls = False
            return list(queryset)

        self.request = request
        if self.count > self.limit and self.template is not None:
            self.display_page_controls = True

        if self.page is not None:
            self.offset = self.limit * self.page
        elif self.page == 0:
            self.offset = 0

        if self.count == 0 or self.offset > self.count:
            return []
        return list(queryset[self.offset:self.offset + self.limit])
