import logging
from django.core.cache import cache

logger = logging.getLogger("general")


def set_blacklisted_token(cache_key, token, timeout=None):
    try:
        blacklisted_token = cache.get(cache_key)

        if not blacklisted_token:
            blacklisted_token = list()

        token_str = str(token)
        if token_str not in blacklisted_token:
            blacklisted_token.append(token_str)

        cache.set(cache_key, blacklisted_token, timeout=timeout)
    except Exception as e:
        logger.error(f'Failed to connect to Redis. Error: {str(e)}')

def get_blacklisted_token(cache_key):
    try:
        return cache.get(cache_key)
    except Exception as e:
        logger.error(f'Failed to connect to Redis. Error: {str(e)}')
