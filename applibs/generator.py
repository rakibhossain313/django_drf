import os
import uuid
import ulid
import random
import string


def generate_unique_id():
    return str(uuid.uuid4().hex).upper()


def generate_ulid():
    return ulid.new().str


def generate_random_password():
    password = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(10))
    return password
