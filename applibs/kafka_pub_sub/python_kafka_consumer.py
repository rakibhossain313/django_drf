import logging
import json
from typing import List, Optional, Union
from kafka import KafkaConsumer

logger = logging.getLogger("general")


def consume_messages_by_python_kafka(topics: List[Union[str, int]], group_id: Optional[Union[str, int]]=None):
    messages = list()

    try:
        consumer = KafkaConsumer(
            *[str(topic) for topic in topics],  # Convert topics to strings (if they are integers),
            bootstrap_servers=['localhost:9092'],
            auto_offset_reset='earliest',
            enable_auto_commit=True,
            group_id=str(group_id) if group_id is not None else None,
            value_deserializer=lambda x: json.loads(x.decode('utf-8'))
        )

        while True:
            msg = consumer.poll(timeout_ms=5000)  # Wait max 5 seconds
            if not msg:
                print(f"================================================")
                logger.error({
                    "request": f"",
                    "response": f"No messages in kafka message queue, exiting...",
                })
                break  # Stop waiting if no messages arrive

            # Process the message
            print(f"================================================")
            logger.info({
                "request": f"",
                "response": f"Received messages from kafka consumer. Messages: {msg}",
            })
            for _, records in msg.items():
                for record in records:
                    messages.append(record.value)

        consumer.close()

        return True, messages
    except Exception as e:
        logger.error({
            "request": f"",
            "response": f"Unexpected error!",
            "error": f"{str(e)}"
        })
        return False, messages
