import logging
import json
from kafka import KafkaProducer

logger = logging.getLogger("general")


def send_message_by_python_kafka(topic: str, data):
    try:
        producer = KafkaProducer(
            bootstrap_servers=['localhost:9092'],
            value_serializer=lambda v: json.dumps(v).encode('utf-8')
        )

        producer.send(topic, data)
        producer.flush()
        producer.close()

        print(f"================================================")
        logger.info({
            "request": f"",
            "response": f"Send messages to kafka publisher. Messages: {data}",
        })

        return True, data
    except Exception as e:
        logger.error({
            "request": f"",
            "response": f"Unexpected error!",
            "error": f"{str(e)}"
        })
        return False, None
