import logging
import json
from typing import List, Optional, Union
from confluent_kafka import (
    Consumer,
    KafkaError
)

logger = logging.getLogger("general")


def consume_messages_by_confluent_kafka(topics: List[Union[str, int]], group_id: Optional[Union[str, int]]=None):
    # Kafka Consumer configuration
    conf = {
        'bootstrap.servers': 'localhost:9092',                          # Kafka broker
        'group.id': str(group_id) if group_id is not None else None,    # Consumer group name
        'auto.offset.reset': 'earliest'                                 # Start reading from the beginning
    }

    # Create a Kafka Consumer
    consumer = Consumer(conf)

    consumer.subscribe([str(topic) for topic in topics])  # Subscribe to the topic

    print("🚀 Listening for messages...")

    messages = list()

    try:

        while True:
            msg = consumer.poll(timeout=3.0)  # Wait for messages (non-blocking)

            if not msg:
                print(f"================================================")
                logger.error({
                    "request": f"",
                    "response": f"No messages in kafka message queue, exiting...",
                })
                break  # Stop waiting if no messages arrive

            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    continue  # Ignore end of partition errors
                else:
                    print(f"================================================")
                    logger.error({
                        "request": f"",
                        "response": f"❌ Error: {msg.error()}",
                    })
                    break

            # Decode and process the message
            message = json.loads(msg.value().decode("utf-8"))

            print(f"================================================")
            logger.info({
                "request": f"",
                "response": f"🔔 Received messages from kafka consumer. Messages: {message}",
            })

            messages.append(message)

        consumer.close()

        return True, messages
    except KeyboardInterrupt as e:
        logger.error({
            "request": f"",
            "response": f"🛑 Consumer stopped by user.",
            "error": f"{str(e)}"
        })
        return False, messages
    except Exception as e:
        logger.error({
            "request": f"",
            "response": f"Unexpected error!",
            "error": f"{str(e)}"
        })
        consumer.close()
        return False, messages
