import logging
import json

from confluent_kafka import Producer

logger = logging.getLogger("general")


# Kafka configuration
conf = {
    'bootstrap.servers': 'localhost:9092'  # Change to your Kafka broker address
}

# Create a Kafka producer
producer = Producer(conf)

def acked(err, msg):
    """Callback function to handle message delivery reports"""
    if err is not None:
        logger.error({
            "request": f"",
            "response": f"❌ Message delivery failed.",
            "error": f"{str(err)}"
        })
    else:
        logger.info({
            "request": f"",
            "response": f"✅ Message delivered to {msg.topic()} [{msg.partition()}] at offset {msg.offset()}",
        })

def send_message_by_confluent_kafka(key, topic: str, value):
    try:
        """Send a message to Kafka"""
        producer.produce(
            topic,
            key=str(key),
            value=json.dumps(value),
            callback=acked  # Handle delivery reports
        )
        producer.flush()  # Ensure the message is sent

        print(f"================================================")
        logger.info({
            "request": f"",
            "response": f"Send messages to kafka publisher. Messages: {value}",
        })

        return True, value
    except Exception as e:
        logger.error({
            "request": f"",
            "response": f"Unexpected error!",
            "error": f"{str(e)}"
        })
        return False, None
