def lang(function):
    """
    Decorator to authenticate a user
    :param: return lng in kwargs field
    """

    def wrap(request, *args, **kwargs):
        accept_language = str(request.META.get('HTTP_ACCEPT_LANGUAGE', 'en')).lower()
        kwargs['lang'] = 'bn' if 'bn' in accept_language else 'en'
        return function(request, *args, **kwargs)

    return wrap
