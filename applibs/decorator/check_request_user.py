from rest_framework.response import Response

from applibs.responses.response import rd
from auth_management.models import User
from auth_management.codes import auth_management


def check_request_user(function):
    """
    Decorator to authenticate a user as a distributor
    :param: return request.user=distributor in kwargs field
    """

    def wrap(request, *args, **kwargs):
        request_user = request.user
        user = User.objects.filter(
            username=request_user.username
        ).last()
        if not user or not user.is_active:
            return Response(
                rd.formatted_error_output(
                    auth_management.REQUEST_USER_NOT_FOUND,
                    kwargs["lang"]
                )
            )

        request.user = user

        return function(request, *args, **kwargs)

    return wrap
