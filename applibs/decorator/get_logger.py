from datetime import datetime
from applibs.generator import generate_ulid


def get_logger(function):
    """
    Decorator to authenticate a user as a back_ops
    :param: return request.user=distributor in kwargs field
    """

    def wrap(request, *args, **kwargs):
        logger = {
            "request_url": request.build_absolute_uri(),
            "request_id": generate_ulid(),
            "request_user": request.user,
            "request_date": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        }

        request.log = logger
        return function(request, *args, **kwargs)

    return wrap
