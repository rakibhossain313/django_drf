import logging

from datetime import (
    datetime,
    timedelta
)

from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.hashers import (
    make_password,
    check_password
)

from jwt.exceptions import InvalidAlgorithmError

from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import (
    NotAuthenticated,
    ValidationError
)
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework_simplejwt.exceptions import (
    InvalidToken,
    TokenError
)

from applibs.generator import generate_ulid
from django_drf import settings

logger = logging.getLogger("general")


class IncomingServiceUser(AnonymousUser):
    def __init__(self, username):
        super().__init__()
        self.username = username

    def __str__(self):
        return f'Service Auth User: {self.username}'


class IncomingServiceAuth(BaseAuthentication):
    def __init__(self):
        super().__init__()

    def authenticate(self, request):
        request.language = request.headers.get('Accept-Language', None)

        auth_header = request.headers.get('Authorization-Service', None)
        if not auth_header:
            raise NotAuthenticated(f'Unauthenticated request')

        token = auth_header.split(" ")[-1]

        try:
            token_backend = TokenBackend(algorithm=settings.ALGORITHM, signing_key=settings.SECRET_KEY)
            decoded_token = token_backend.decode(token=token, verify=True)

            exp_datetime = int(decoded_token['expire_at'])
            current_datetime = int(datetime.utcnow().timestamp())

            if exp_datetime < current_datetime:
                raise ValidationError(f'Expired access token')

            if decoded_token["username"] != settings.INTER_SERVICE_USER:
                raise NotAuthenticated(f'Unauthenticated user')

            if not check_password(settings.INTER_SERVICE_PASSWORD, decoded_token["password"]):
                raise NotAuthenticated(f'Unauthenticated user password')

            return IncomingServiceUser(decoded_token["username"]), "CH_SERVICE_AUTH"
        except InvalidToken as e:
            logger.error(e)
            raise ValidationError(f'Invalid access token')
        except TokenError as e:
            logger.error(e)
            raise ValidationError(f'Access token error or expired')
        except Exception as e:
            logger.error(e)
            raise ValidationError(f'Error on decoding token: {str(e)}')


class OutgoingServiceAuth():
    def __init__(self):
        super().__init__()

    def service_auth(self, service_user=None, service_password=None):
        try:
            expire_at = int((datetime.utcnow() + timedelta(hours=1)).timestamp())
            issue_at = int(datetime.utcnow().timestamp())

            service_auth_payload = {
                "expire_at": expire_at,
                "issue_at": issue_at,
                "jti": generate_ulid(),
                "username": service_user,
                "password": make_password(service_password),
            }

            token_backend = TokenBackend(algorithm=settings.ALGORITHM, signing_key=settings.SECRET_KEY)
            token = token_backend.encode(payload=service_auth_payload)

            data = {
                "access": token,
                "access_token_expired_at": str(expire_at),
            }
            return data
        except InvalidAlgorithmError as e:
            logger.error(f'Invalid algorithm error: {e}')
            raise ValueError(f'The algorithm specified is invalid.')
        except TokenError as e:
            logger.error(f'Token encoding error: {e}')
            raise ValidationError(f'There was an issue encoding the token.')
        except TypeError as e:
            logger.error(f'Type error in token payload: {e}')
            raise TypeError(f'There was a type error in the token payload. Ensure data types are correct.')
        except ValueError as e:
            logger.error(f'Value error: {e}')
            raise ValueError(f'Invalid value encountered in payload or token encoding.')
        except Exception as e:
            logger.error(f'An unexpected error occurred during token creation: {e}')
            raise Exception(f'An unexpected error occurred while generating the token.')


incoming_service_auth = IncomingServiceAuth()
outgoing_service_auth = OutgoingServiceAuth()
