import pandas as pd

from django_drf import settings

"""
Command Line: python manage.py runscript bd_info_hierarchy_scrape
"""

# Define a function to clean each cell
def clean_text(text):
    if isinstance(text, str):  # Only apply cleaning to string types
        text = text.replace('\t', '')  # Remove tab characters
        text = text.replace('\n', '')  # Remove newline characters
        text = ' '.join(text.split())  # Remove extra spaces
    return text

def run():
    # Load the original CSV file
    read_csv_file_path = settings.BASE_DIR / 'scripts/files/bd_info_hierarchy.csv'

    df = pd.read_csv(read_csv_file_path)

    # # Apply the cleaning function to each cell in the DataFrame
    # df = df.applymap(clean_text)
    # # Display the cleaned DataFrame
    # print(df)

    # Select specific columns by index (e.g., columns 0, 1, 2, 3, 4, 5, 6, 7, 8, and 9 for the example data)
    # Adjust the column indices as needed
    selected_columns = df.iloc[:, [2, 4, 6, 8]]  # Modify indices as needed

    print("Selected Columns: \n", selected_columns)

    selected_columns.drop_duplicates(inplace=True)

    # Save the selected columns to a new CSV file
    write_csv_file_path = settings.BASE_DIR / 'scripts/files/final_bd_info_hierarchy.csv'
    selected_columns.to_csv(write_csv_file_path, index=False, header=True)
