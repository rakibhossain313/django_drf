import csv

from django.conf import settings
from django.db import connections

"""
Command Line: python manage.py runscript bank_info_migrate
"""

def run():
    print("Running migrations for bank info's...")

    # Example of a raw SQL query with multiple columns (replace with your actual query)
    raw_query = """
        SELECT * FROM distributor_bankroute
        -- WHERE bank_alias = %s AND dist_code = %s
    """

    # Parameters for the query: first for is_active, second for username
    # params = ['UCBL', '112']

    bank_info_list = list()

    # Using connection to execute the raw SQL query
    with connections['db2'].cursor() as cursor:
        cursor.execute(raw_query)
        # cursor.execute(raw_query, params)
        rows = cursor.fetchall()

        # Process the results (rows is a list of tuples)
        for row in rows:
            bank_info = dict()
            bank_info['dist_code'] = row[1]
            bank_info['dist_name'] = row[2]
            bank_info['bank_code'] = row[3]
            bank_info['bank_name'] = row[4]
            bank_info['bank_alias'] = row[5]
            bank_info['branch_name'] = row[6]
            bank_info['branch_code'] = row[7]
            bank_info['routing_number'] = row[8]
            bank_info_list.append(bank_info)
        print("Bank Infos: ", bank_info_list)

    # File path for the CSV file
    csv_file_path = settings.BASE_DIR / 'scripts/files/bank_info.csv'
    with open(csv_file_path, mode='w', newline='', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file)

        # Write the header row (column names)
        writer.writerow(['dist_code', 'dist_name', 'bank_code', 'bank_name', 'bank_alias', 'branch_name', 'branch_code', 'routing_number'])
        for bank_info in bank_info_list:
            writer.writerow([
                bank_info['dist_code'],
                bank_info['dist_name'],
                bank_info['bank_code'],
                bank_info['bank_name'],
                bank_info['bank_alias'],
                bank_info['branch_name'],
                bank_info['branch_code'],
                bank_info['routing_number'],
            ])

    print("Finished migrations.")
