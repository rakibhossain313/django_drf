import csv

from django_drf import settings
from car.models import BangladeshHierarchyInfo

"""
Command Line: python manage.py runscript bd_info_hierarchy_entry
"""

def run():
    print("Running migrations for BD hierarchy info's...")

    # File path for the CSV file
    csv_file_path = settings.BASE_DIR / 'scripts/files/final_bd_info_hierarchy.csv'
    with open(csv_file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)  # skips the first(header) line

        batch_size = 1000
        line_count = 0
        bd_hierarchy_info_list = []
        for column in csv_reader:
            line_count += 1

            division = column[0].lower().strip()
            district = column[1].lower().strip()
            upazilla = column[2].lower().strip()
            union = column[3].lower().strip()

            agent = BangladeshHierarchyInfo(
                division=division,
                district=district,
                upazilla=upazilla,
                union=union,
            )

            bd_hierarchy_info_list.append(agent)

            print("Appending line number: ", line_count)

        _ = BangladeshHierarchyInfo.objects.bulk_create(bd_hierarchy_info_list, batch_size)
        print(f'Bulk agent created per batch: {batch_size}')

    print("Finished migrations.")
