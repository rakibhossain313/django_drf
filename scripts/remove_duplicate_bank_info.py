import pandas as pd

from django_drf import settings

"""
Command Line: python manage.py runscript remove_duplicate_bank_info
"""

def run():
    # Load the original CSV file
    read_csv_file_path = settings.BASE_DIR / 'scripts/files/bank_info.csv'

    df = pd.read_csv(read_csv_file_path)

    print("Data Frame: \n", df)

    df.drop_duplicates(inplace=True)

    # Save the selected columns to a new CSV file
    write_csv_file_path = settings.BASE_DIR / 'scripts/files/final_bank_info.csv'
    df.to_csv(write_csv_file_path, index=False, header=True)
