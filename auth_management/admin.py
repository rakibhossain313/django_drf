from django.contrib import admin

from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'username',
        'email',
        'first_name',
        'last_name',
        'is_superuser',
        'is_staff',
        'is_active',
        'last_login',
        'created_at',
        'updated_at',
    ]
    list_filter = [
        'is_superuser',
        'is_staff',
        'is_active',
        'last_login',
        'created_at',
        'updated_at',
    ]
    raw_id_fields = ['groups', 'user_permissions']
    date_hierarchy = 'created_at'
    search_fields = [
        'username',
        'email',
        'first_name',
        'last_name',
    ]
