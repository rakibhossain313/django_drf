import logging
from datetime import datetime, timedelta

from django.utils.decorators import method_decorator
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_xml.parsers import XMLParser
from rest_framework_xml.renderers import XMLRenderer

from applibs.connect_cache import (
    set_blacklisted_token,
    get_blacklisted_token
)
from applibs.decorator.get_logger import get_logger
from applibs.decorator.message_lang import lang
from applibs.responses.response import rd
from applibs.service.service_auth import IncomingServiceAuth
from auth_management.codes import auth_management
from auth_management.serializers import (
    LoginSerializer,
    LoginRefreshSerializer
)

logger = logging.getLogger("general")


class UserLoginView(APIView):
    permission_classes = [AllowAny]

    def __init__(self):
        super().__init__()
        self.language = "en"
        self.data = None
        self.serializer = LoginSerializer

    @method_decorator([
        lang,
        get_logger
    ])
    def post(self, request, **kwargs):
        serializer = self.serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                **rd.formatted_error_output(
                    code=auth_management.INVALID_INPUT,
                    language=self.language,
                    errors=serializer.errors
                )
            )

        try:
            response_data = {
                "access": serializer.validated_data.get('access'),
                "access_token_expired_at": serializer.validated_data.get('access_token_expired_at'),
                "refresh": serializer.validated_data.get('refresh'),
                "refresh_token_expired_at": serializer.validated_data.get('refresh_token_expired_at'),
                "user_details": {
                    "email": serializer.validated_data.get('email'),
                    "first_name": serializer.validated_data.get('first_name'),
                    "last_name": serializer.validated_data.get('last_name'),
                    "is_active": serializer.validated_data.get('is_active'),
                },
            }

            logger.info({
                "request": self.request.log,
                "response": response_data["user_details"],
                "error": ""
            })
            return Response(
                **rd.formatted_success_output(
                    code=auth_management.LOGIN_SUCCESS,
                    language=self.language,
                    data=response_data,
                )
            )

        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "Error occurred",
                "error": e
            })
            return Response(
                **rd.formatted_error_output(
                    code=auth_management.INTERNAL_SERVER_ERROR,
                    language=self.language,
                )
            )


class UserLoginRefreshView(APIView):
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super(UserLoginRefreshView, self).__init__()
        self.language = "en"
        self.data = None
        self.serializer = LoginRefreshSerializer

    @method_decorator([
        lang,
        get_logger
    ])
    def post(self, request, **kwargs):
        serializer = self.serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                **rd.formatted_error_output(
                    code=auth_management.INVALID_INPUT,
                    language=self.language,
                    errors=serializer.errors
                )
            )

        try:
            data = {
                "access": serializer.validated_data.get('access'),
                "access_token_expired_at": serializer.validated_data.get("access_token_expired_at"),
            }

            logger.info({
                "request": self.request.log,
                "response": "Successfully generated refresh token",
                "error": ""
            })
            return Response(
                **rd.formatted_success_output(
                    code=auth_management.LOGIN_SUCCESS,
                    language="en",
                    data=data,
                )
            )
        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "error occurred on generating refresh token",
                "error": str(e)
            })
            return Response(
                **rd.formatted_error_output(
                    code=auth_management.LOGIN_FAILED,
                    language="en",
                    errors=str(e),
                )
            )


class UserLogoutView(APIView):
    permission_classes = [IsAuthenticated]

    def __init__(self):
        super(UserLogoutView, self).__init__()
        self.language = "en"

    @method_decorator([
        lang,
        get_logger
    ])
    def get(self, request, **kwargs):
        try:
            token = request.auth
            token_payload = request.auth.payload

            # calculate remaining time before expiration
            remaining_time = token_payload['exp'] - int(datetime.utcnow().timestamp())

            set_blacklisted_token(cache_key="blacklisted", token=token, timeout=remaining_time)

            # blacklisted_token = get_blacklisted_token(cache_key="blacklisted")
            # print("blacklisted token =============>", blacklisted_token)

            return Response(
                **rd.formatted_success_output(
                    code=auth_management.LOGOUT_SUCCESS,
                    language=self.language,
                    data={},
                )
            )

        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "Error occurred",
                "error": e
            })
            return Response(
                **rd.formatted_error_output(
                    code=auth_management.LOGOUT_FAILED,
                    language=self.language,
                )
            )
