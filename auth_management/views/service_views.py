import logging
from datetime import datetime

from django.utils.decorators import method_decorator

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)

from applibs.decorator.get_logger import get_logger
from applibs.decorator.message_lang import lang
from applibs.responses.response import rd
from auth_management.codes import auth_management
from auth_management.serializers import (
    ServiceLoginSerializer
)

logger = logging.getLogger("general")


class ServiceLoginView(APIView):
    permission_classes = [AllowAny]

    def __init__(self):
        super(ServiceLoginView, self).__init__()
        self.language = "en"
        self.serializer = ServiceLoginSerializer

    @method_decorator([
        lang,
        get_logger
    ])
    def post(self, request, **kwargs):
        serializer = self.serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                **rd.formatted_error_output(
                    code=auth_management.INVALID_LOGIN_INPUT,
                    language=self.language,
                    errors=serializer.errors
                )
            )

        try:
            response_data = {
                "access": serializer.validated_data.get('access'),
                "access_token_expired_at": serializer.validated_data.get('access_token_expired_at'),
            }

            return Response(
                **rd.formatted_success_output(
                    code=auth_management.SERVICE_LOGIN_SUCCESS,
                    language=self.language,
                    data=response_data,
                )
            )
        except Exception as e:
            logger.error({
                "request": self.request.log,
                "response": "error occurred on service login",
                "error": str(e)
            })
            return Response(
                **rd.formatted_error_output(
                    code=auth_management.SERVICE_LOGIN_FAILED,
                    language=self.language,
                    errors=str(e)
                )
            )
