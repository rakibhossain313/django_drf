from django.urls import path, include

app_name = 'auth_management'

urlpatterns = [
    path('web/', include('auth_management.urls.web_urls'), name='auth_management_web_urls'),
    path('service/', include('auth_management.urls.service_urls'), name='auth_management_service_urls'),
]
