from django.urls import path
from auth_management.views.service_views import ServiceLoginView

urlpatterns = [
    # service apis
    path('v1/login/', ServiceLoginView.as_view(), name='service-login-view'),
]
