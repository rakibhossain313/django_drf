from django.urls import path

from auth_management.views.auth_views import (
    UserLoginView,
    UserLoginRefreshView,
    UserLogoutView,
)

urlpatterns = [
    # web apis
    path('v1/login/', UserLoginView.as_view(), name='user-login-view'),
    path('v1/refresh/', UserLoginRefreshView.as_view(), name='user-login-refresh-view'),
    path('v1/logout/', UserLogoutView.as_view(), name='user-logout-view'),
]
