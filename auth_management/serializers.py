import logging

from django.contrib.auth import authenticate
from rest_framework import serializers
from rest_framework_simplejwt.serializers import (
    TokenObtainPairSerializer,
    TokenRefreshSerializer
)
from rest_framework_simplejwt.state import token_backend
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError

from applibs.connect_cache import get_blacklisted_token
from applibs.service.service_auth import outgoing_service_auth
from django_drf import settings

logger = logging.getLogger("general")


class LoginSerializer(TokenObtainPairSerializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True, write_only=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None

    """
    Custom token serializer to customize the payload.
    """

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Customize the payload according to your requirements
        token['user_id'] = user.id
        token['username'] = user.username
        token['email'] = user.email
        # Add more custom claims here

        return token

    def validate(self, attrs):
        try:
            username = attrs.get("username", None)
            password = attrs.get("password", None)

            self.user = authenticate(username=username, password=password)

            refresh = self.get_token(self.user)

            blacklisted_token = get_blacklisted_token("blacklisted")
            if blacklisted_token and refresh.access_token in blacklisted_token:
                raise serializers.ValidationError("Your token is blacklisted")

            attrs["refresh"] = str(refresh)
            attrs["refresh_token_expired_at"] = str(refresh['exp'])
            attrs["access"] = str(refresh.access_token)
            attrs["access_token_expired_at"] = str(refresh.access_token['exp'])
            attrs["email"] = self.user.email
            attrs["first_name"] = self.user.first_name
            attrs["last_name"] = self.user.last_name
            attrs["is_active"] = self.user.is_active

            return attrs
        except InvalidToken as e:
            logger.error(e)
            raise serializers.ValidationError('Invalid access token')
        except TokenError as e:
            logger.error(e)
            raise serializers.ValidationError('Access token error or expired')
        except Exception as e:
            logger.error(e)
            raise serializers.ValidationError(f'Error on generating token: {str(e)}')


class LoginRefreshSerializer(TokenRefreshSerializer):
    def validate(self, attrs):
        try:
            data = super(LoginRefreshSerializer, self).validate(attrs)
            token_backend = TokenBackend(algorithm=settings.ALGORITHM, signing_key=settings.SECRET_KEY)
            decoded_payload = token_backend.decode(token=data['access'], verify=True)
            data['access_token_expired_at'] = decoded_payload['exp']
            return data
        except InvalidToken as e:
            logger.error(e)
            raise serializers.ValidationError('Invalid access token')
        except TokenError as e:
            logger.error(e)
            raise serializers.ValidationError('Access token error or expired')
        except Exception as e:
            logger.error(e)
            raise serializers.ValidationError(f'Error on decoding token: {str(e)}')


class ServiceLoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100, required=True)
    password = serializers.CharField(max_length=100, required=True, write_only=True)

    def validate(self, attrs):
        username = attrs.get("username", None)
        password = attrs.get("password", None)

        return outgoing_service_auth.service_auth(service_user=username, service_password=password)
