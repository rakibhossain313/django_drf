from rest_framework import status
from applibs.responses.response import CodeObject


INVALID_INPUT = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_II_400",
    state_message={"en": "Invalid input", "bn": ""},
)

INVALID_LOGIN_INPUT = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="CHC_II_400",
    state_message={"en": "Invalid login input!", "bn": ""},
)

INTERNAL_SERVER_ERROR = CodeObject(
    http_status=status.HTTP_500_INTERNAL_SERVER_ERROR,
    state_code="DDRF_ISE_500",
    state_message={"en": "Internal server error", "bn": ""},
)

INVALID_TOKEN = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_IT_400",
    state_message={"en": "Invalid token", "bn": ""},
)

LOGIN_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_LIS_200",
    state_message={"en": "User login successful", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

LOGIN_FAILED = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_LIF_400",
    state_message={"en": "User login failed", "bn": ""},
)

LOGOUT_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_LOS_200",
    state_message={"en": "User logout successful", "bn": ""},
    state_message_params={"en": "State message params", "bn": ""},
)

LOGOUT_FAILED = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_LOF_400",
    state_message={"en": "User logout failed", "bn": ""},
)

SERVICE_LOGIN_SUCCESS = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="CHC_SLS_200",
    state_message={"en": "Service login successful!", "bn": ""},
)

SERVICE_LOGIN_FAILED = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="CHC_SLF_400",
    state_message={"en": "Service login failed!", "bn": ""},
)

REQUEST_USER_FOUND = CodeObject(
    http_status=status.HTTP_200_OK,
    state_code="DDRF_RUF_200",
    state_message={"en": "Request user found", "bn": ""},
)

REQUEST_USER_NOT_FOUND = CodeObject(
    http_status=status.HTTP_400_BAD_REQUEST,
    state_code="DDRF_RUNF_400",
    state_message={"en": "Request user not found", "bn": ""},
)
