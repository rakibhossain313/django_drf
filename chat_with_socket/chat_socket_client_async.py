import os
import socket
import asyncio


# Define the socket file location (should match the server's location)
socket_file = "/home/rakibhossain/Projects/django_drf/chat_with_socket/chat.sock"


async def client():
    reader, writer = await asyncio.open_unix_connection(socket_file)

    print("Connected to the server. Type 'exit' to quit.")

    while True:
        message = input("Enter message: ")

        if message.lower() == 'exit':
            print("Closing connection...")
            break

        writer.write(message.encode())
        await writer.drain()

        response = await reader.read(1024)
        print(f"Server response: {response.decode()}")

    writer.close()
    await writer.wait_closed()


asyncio.run(client())
