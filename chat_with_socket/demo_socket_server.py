import socket
import os

# Define the socket file location
socket_file = "/home/rakibhossain/Projects/django_drf/chat_with_socket/demo.sock"

# Remove the socket file if it already exists
try:
    os.unlink(socket_file)
except OSError:
    if os.path.exists(socket_file):
        raise

# Create a Unix socket
server_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

# Bind the socket to the file
server_socket.bind(socket_file)

print(f"Socket created at: {socket_file}")

# Listen for incoming connections
server_socket.listen(1)

print("Waiting for a connection...")

# Accept a connection (blocking call)
connection, client_address = server_socket.accept()

try:
    print("Connection established with:", client_address)

    # Send a welcome message to the client
    welcome_message = "Welcome to the Unix socket server!\n"
    connection.sendall(welcome_message.encode('utf-8'))

    # Receive data from the client
    while True:
        data = connection.recv(1024)  # Buffer size of 1024 bytes
        if not data:
            break  # No more data, exit the loop
        print("Received:", data.decode('utf-8'))

        # Echo the received data back to the client
        connection.sendall(data)
finally:
    connection.close()
    server_socket.close()
