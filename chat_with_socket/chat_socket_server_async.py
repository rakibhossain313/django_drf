import os
import socket
import asyncio


# Define the socket file location
socket_file = "/home/rakibhossain/Projects/django_drf/chat_with_socket/chat.sock"

# Remove the socket file if it already exists
try:
    os.unlink(socket_file)
except OSError:
    if os.path.exists(socket_file):
        raise


async def handle_client(reader, writer):
    print("New client connected!")

    while True:
        data = await reader.read(1024)  # Read data from client (async)
        if not data:
            break  # Exit loop when client disconnects

        message = data.decode()
        print(f"Received: {message}")

        # Send response back to client
        response = f"Server received: {message}"
        writer.write(response.encode())
        await writer.drain()  # Ensure data is sent completely

    print("Client disconnected.")
    writer.close()  # Close the writer stream
    await writer.wait_closed()  # Wait until fully closed


async def main():
    server = await asyncio.start_unix_server(handle_client, socket_file)
    print(f"Server listening on {socket_file}...")
    async with server:
        await server.serve_forever()


asyncio.run(main())
