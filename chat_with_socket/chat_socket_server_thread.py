import os
import socket
import threading


# Define the socket file location
socket_file = "/home/rakibhossain/Projects/django_drf/chat_with_socket/chat.sock"

# Remove the socket file if it already exists
try:
    os.unlink(socket_file)
except OSError:
    if os.path.exists(socket_file):
        raise

# Create a Unix socket
server_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

# Bind the socket to the file
server_socket.bind(socket_file)
print(f"Socket created at: {socket_file}")

# Listen for incoming connections
server_socket.listen(1)
print("Waiting for a connection...")

def handle_client(connection):
    # Receive data from the client
    while True:
        data = connection.recv(1024)  # Buffer size of 1024 bytes
        if not data:
            break  # No more data, exit the loop
        print("Received:", data.decode('utf-8'))

        # Echo the received data back to the client
        connection.sendall(data)

    connection.close()
    print("Client disconnected.")

try:
    while True:
        # Accept a connection (blocking call)
        connection, client_address = server_socket.accept()
        print(f"Connection established with new client!")

        # Send a welcome message to the client
        welcome_message = "Welcome to the Unix socket server!\n"
        connection.sendall(welcome_message.encode('utf-8'))

        # handle_client(connection)

        client_thread = threading.Thread(target=handle_client, args=(connection,))
        client_thread.start()
finally:
    server_socket.close()
