import asyncio

async def task1():
    print("Task 1 started")
    await asyncio.sleep(3)
    print("Task 1 completed")

async def task2():
    print("Task 2 started")
    await asyncio.sleep(2)
    print("Task 2 completed")

async def main():
    # Run both tasks concurrently and total time taken 3 seconds (not 5)
    await asyncio.gather(task1(), task2())

asyncio.run(main())  # Run the main function
