import os
import socket


# Define the socket file location (should match the server's location)
socket_file = "/home/rakibhossain/Projects/django_drf/chat_with_socket/chat.sock"

# Create a Unix socket
client_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
print("Connected to the server. Type 'exit' to quit.")

try:
    # Connect to the socket
    client_socket.connect(socket_file)

    # Receive the welcome message
    welcome_message = client_socket.recv(1024)
    print("Server:", welcome_message.decode('utf-8'))

    while True:
        message = input("Enter message: ")

        if message.lower() == 'exit':
            print("Closing connection...")
            break

        # Send message to server
        client_socket.sendall(message.encode())

        # Receive response from server
        response = client_socket.recv(1024)
        print(f"Server response: {response.decode()}")

    # Close the connection
    client_socket.close()
finally:
    client_socket.close()
