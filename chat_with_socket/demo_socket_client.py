import socket
import os

# Define the socket file location (should match the server's location)
socket_file = "/home/rakibhossain/Projects/django_drf/chat_with_socket/demo.sock"

# Create a Unix socket
client_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

try:
    # Connect to the socket
    client_socket.connect(socket_file)

    # Receive the welcome message
    welcome_message = client_socket.recv(1024)
    print("Server:", welcome_message.decode('utf-8'))

    # Send a message to the server
    message = "Hello from the client!"
    client_socket.sendall(message.encode('utf-8'))

    # Receive the echo from the server
    response = client_socket.recv(1024)
    print("Server echoed:", response.decode('utf-8'))

finally:
    client_socket.close()
